var app = ons.bootstrap();

app.directive('focusMe', function ($timeout) {
  return {
    link: function (scope, element, attrs) {
      scope.$watch(attrs.focusMe, function (value) {
        //if(value === true) {
        //console.log('value=',value);
        //$timeout(function() {
        element[0].focus();
        scope[attrs.focusMe] = false;
        //});
        //}
      });
    }
  };
});

app.directive('onLongPress', function ($timeout) {
  return {
    restrict: 'A',
    link: function ($scope, $elm, $attrs) {
      $elm.bind('touchstart', function (evt) {
        // Locally scoped variable that will keep track of the long press
        $scope.longPress = true;

        // We'll set a timeout for 600 ms for a long press
        $timeout(function () {
          if ($scope.longPress) {
            // If the touchend event hasn't fired,
            // apply the function given in on the element's on-long-press attribute
            $scope.$apply(function () {
              $scope.$eval($attrs.onLongPress)
            });
          }
        }, 600);
      });

      $elm.bind('touchend', function (evt) {
        // Prevent the onLongPress event from firing
        $scope.longPress = false;
        // If there is an on-touch-end function attached to this element, apply it
        if ($attrs.onTouchEnd) {
          $scope.$apply(function () {
            $scope.$eval($attrs.onTouchEnd)
          });
        }
      });
    }
  };
})

app.filter("split", function() {
  return function(input, splitChar, splitIndex) {
    // do some bounds checking here to ensure it has that index
    return input.split(splitChar)[splitIndex];
  };
});

app.service("$data", function() {
  var user = {};
  var folder = {};
  var notas = [];
  var nota = {};
  var firstTime = false;
  return {
    getUser: function() {
      return user;
    },
    setUser: function(u) {
      user = u;
    },
    getServer: function() {
      return server;
    },
    setFolder: function(l) {
      folder = l;
    },
    getFolder: function() {
      return folder;
    },
    setNotas: function(n) {
      notas = n;
    },
    getNotas: function() {
      return notas;
    },
    setNota: function(n) {
      nota = n;
    },
    getNota: function() {
      return nota;
    },
    getFirst: function(){
      return firstTime;
    },
    setFirst: function(f){
      firstTime = f;
    }
  };
});

app.controller("MainController", function($scope, $window) {
  $scope.fw = "0";
  
  $scope.getDateTime = function() {
    var d = new Date();
    return {
      time: d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds(),
      date:
        ("0" + parseInt(d.getDate())).slice(-2) +
        "-" +
        ("0" + parseInt(d.getMonth() + 1)).slice(-2) +
        "-" +
        d.getFullYear()
    };
  };
  $scope.generateID = function() {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };
  ons.ready(function() {
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyD9iUl-RprgsYhFuh0bOdAOjEiC4X3AwTk",
      authDomain: "bionic-client-798.firebaseapp.com",
      databaseURL: "https://bionic-client-798.firebaseio.com",
      projectId: "bionic-client-798",
      storageBucket: "",
      messagingSenderId: "411517468075"
    };
    firebase.initializeApp(config);
  });
  $scope.storage = window.localStorage;
  $scope.loading = false;
});

app.controller("HomeController", function($scope, $data, $window) {
  $scope.usuario = $data.getUser();
  $scope.database = firebase.database();
  $scope.notas = [];
  $scope.searching = false;
  $scope.searchText = "";
  $scope.search=function(){
    $scope.searching ? $scope.searching = false : $scope.searching=true;
    $scope.searchText = "";
  }
  var w = $window.innerWidth;
  if (w > 0 && w < 550) $scope.fw = "100%";
  if (w > 549 && w < 850) $scope.fw = "50%";
  if (w > 849 && w < 1480) $scope.fw = "33%";
  if (w > 1479) $scope.fw = "20%";

  angular.element($window).on("resize", function () {
    console.log($window.innerWidth);
    var w = $window.innerWidth;
    if (w > 0 && w < 550) $scope.fw = "100%";
    if (w > 549 && w < 850) $scope.fw = "50%";
    if (w > 849 && w < 1480) $scope.fw = "33%";
    if (w > 1479) $scope.fw = "20%";

    $scope.$apply();
  });
  /*$scope.database
    .ref("notes/")
    .child($scope.usuario.id + "/leafs/")
    .orderByChild("titulo")
    .on("value", function(snapshot) {
      console.log(snapshot.val());
      if (snapshot.val() !== null) {
        $scope.notas = snapshot.val();
      } else {
        $scope.notas = [];
      }
      $scope.$apply();
    });*/
    if ($scope.storage.getItem("notas") !== null) {
      $scope.notas=JSON.parse($scope.storage.getItem("notas"));
      
    } else {
      $scope.database
        .ref("notes/")
        .child($scope.usuario.id + "/leafs/")
        .orderByChild("titulo")
        .once("value")
        .then(function(snapshot) {
          console.log(snapshot.val());
          $scope.notas = snapshot.val();
          $scope.$apply();
          $scope.storeFirebase();
        });
    }
  $scope.storeFirebase=function(){
    console.log("Guardando...")
    $scope.storage.setItem("notas", JSON.stringify(angular.copy($scope.notas)));
    firebase
      .database()
      .ref("notes/" + $scope.usuario.id + "/" + "leafs/")
      .set(angular.copy($scope.notas));
  }
  $scope.verNota = function(n) {
    if ($scope.selection) {
      n.selected?n.selected=false:n.selected=true;
    }else{
      $data.setNota(n);
      $data.setNotas($scope.notas);
      $scope.navi.pushPage("nota.html", { animation: "slide-ios" });
    }
  };
  $scope.editarColor = function(nota, $event) {
    $scope.colorPicker.show($event.target);
    $scope.nota = nota;
  };
  $scope.addNote = function() {
    var date = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");
    var id = Date.now();
    var s = {
      bg: "a-e-gris",
      //text: "Nueva nota " + $scope.getDateTime().date,
      text:"",
      id: id,
      created: $scope.getDateTime(),
      updated: $scope.getDateTime(),
      device: []
    };
    $scope.notas.push(s);
    for(i =0;i<$scope.notas.length;i++){
      if($scope.notas[i].id==s.id){
        $data.setFirst(true);
        $scope.verNota($scope.notas[i]);
      }
    }
    $scope.storeFirebase();
    // agregar link
    /*firebase
      .database()
      .ref("notes/" + $scope.usuario.id + "/" + "leafs/" + id)
      .set(s);
    $scope.verNota(s);*/
  };
  $scope.navi.on("prepop",function(ev){
    //$scope.storeFirebase();
    $data.setFirst(false);
    $scope.selection = false;
    $scope.selected = [];
  });
  ons.ready(function(){
    ons.setDefaultDeviceBackButtonListener(function(ev){
      console.log(ev);
      if ($scope.selection||$scope.searching) {
        $scope.searching=false;
        $scope.searchText = "";
        $scope.quitSelection();
        $scope.$apply();
      }else{
        navigator.app.exitApp();
      }
    });
  });
  $scope.filters={
    bg:""
  }
  $scope.selection=false;
  $scope.selected=[];
  $scope.lp=function(nota){
    $scope.selection ? ($scope.selection = true) : (nota.selected = true); 
    $scope.selection = true;
  }
  $scope.quitSelection=function(){
    for(i=0;i<$scope.notas.length;i++){
      $scope.notas[i].selected=false;
    }
    $scope.selection=false;
  };
  $scope.deleteSelected = function () {
    ons.notification.confirm({message:"Seguro que quieres eliminar los elementos seleccionados?",callback:function(r){
      if (r>0) {
        $scope.notas = $scope.notas.filter(function (item) {
          return !item.selected
        });
        $scope.selection = false;
        $scope.$apply();
        $scope.storeFirebase();
      }
    }
  })
  };
  $scope.salir=function(){
    $scope.storage.removeItem("notas");
    $scope.storage.removeItem("sesion");
    $scope.navi.replacePage("loading.html")
  }
});

app.controller("NotaController", function($scope, $data) {
  $scope.usuario = $data.getUser();
  $scope.nota = $data.getNota();
  $scope.notas = $data.getNotas();
  $scope.edit=$data.getFirst();
  $scope.database = firebase.database();
  $scope.storeFirebase = function () {
    console.log("Guardando2...",$scope.notas);
    $scope.storage.setItem("notas", JSON.stringify(angular.copy($scope.notas)));
    firebase
      .database()
      .ref("notes/" + $scope.usuario.id + "/" + "leafs/")
      .set(angular.copy($scope.notas));
  }
  
});

app.controller("LoadController", function($scope, $data) {
  $scope.sign = function() {
    $scope.loading = true;
    // Initialize Firebase

    var provider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithRedirect(provider)
      .then(function() {
        firebase
          .auth()
          .getRedirectResult()
          .then(function(result) {
            // This gives you a Google Access Token.
            // You can use it to access the Google API.
            var token = result.credential.accessToken;
            console.log(token);

            // The signed-in user info.
            var user = result.user;
            console.log(user);
            var profile = user.providerData[0];
            var u = {
              id: profile.uid,
              name: profile.displayName,
              image: profile.photoURL,
              email: profile.email,
              token: token
            };
            $data.setUser(u);
            $scope.storage.setItem("sesion", JSON.stringify(u));
            $scope.navi.replacePage("home.html", { animation: "lift-ios" });
            // ...
          })
          .catch(function(error) {
            // Handle Errors here.
            console.log(error);

            var errorCode = error.code;
            var errorMessage = error.message;
          });
      });
  };
  
});

app.controller("PreController",function($scope,$data){
  ons.ready(function () {
    if ($scope.storage.getItem("sesion") !== null) {
      $data.setUser(JSON.parse($scope.storage.getItem("sesion")));
      setTimeout(function () {
        $scope.navi.replacePage("home.html", { animation: "lift-ios" });
      }, 50);
    } else {
      //if (!$scope.loading) $scope.sign();
      //$scope.loading = false;
      setTimeout(function() {
        $scope.navi.replacePage("loading.html", { animation: "lift-ios" });
      }, 50);
    }
  });
});